/*Order of execution to run script and record:
1. Hit "Ctrl/Cmd - Enter" anywhere inside the "LOAD SYNTH FUNCTIONS" parenthesis on line 52,
2. Click on the same line as "START RECORDING" on line 49 and hit "Ctrl/Cmd - Enter",
3. Hit "Ctrl/Cmd - Enter" anywhere inside the "TASK" parenthesis on line 12...
The record file location will be found in the post window...
Leave out #2 to run the script without recording...
Important Note: You must load the appropriate Timewave Synthesizer program and run the synth by pressing the "synth0" button at least once before this script can run.
Press "Ctrl/Cmd - Period" to stop synth/free the sever.

You can compose a piece, and find guides for doing so, inside the "//3 - TASK" function. */

((//3 - TASK
	t = Task({/*trace*/ ~st = 0; ~trace = {("event-"++(~st = ~st + 1).asString).postln;}; ~number_of_synths.do(x = 0; {("vol"++(x=x+1)).asSymbol.envirPut(/*volume*/ ~vol)});
		1.do({

			//duration of each chord
			a = 8;

			//copy and paste "saved" chords from the "copy" button inside the synthesizer program here:
			//choose to change each duration individually by changing the "a" variable in the code below this line, as well as "synthopen/synthflow"
			[~f1=525.7244292648,~f2=646.87931910571,~f3=882.91913643716,~f4=1181.3955692862,~f5=706.55967076687,~f6=711.25161146077,~f7=977.22674582697,~f8=1325.0082712923/2]; ~synthopen.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=1100.8670554563,~f5=1316.2675385788,~f6=1316.2675385788,~f7=1316.2675385788,~f8=1316.2675385788/2]; ~synthflow.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=1100.8670554563,~f5=1316.2675385788,~f6=1051.0271787501,~f7=1051.0271787501,~f8=1051.0271787501/2]; ~synthflow.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=1100.8670554563,~f5=1316.2675385788,~f6=937.1102649523,~f7=937.1102649523,~f8=937.1102649523]; ~synthflow.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=1100.8670554563,~f5=1316.2675385788,~f6=824.55305817848,~f7=824.55305817848,~f8=822.73593638317]; ~synthflow.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=1100.8670554563,~f5=1316.2675385788,~f6=824.55305817848,~f7=1037.2062139187,~f8=1037.2062139187]; ~synthflow.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=1100.8670554563,~f5=1316.2675385788,~f6=554.31101031015,~f7=554.31101031015,~f8=554.31101031015]; ~synthflow.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=521.10543213641,~f5=521.10543213641,~f6=521.10543213641,~f7=521.10543213641,~f8=521.10543213641]; ~synthflow.value; ~trace.value; (a).wait;
			[~f1=525.7244292648,~f2=646.87931910571,~f3=882.91913643716,~f4=1181.3955692862,~f5=706.55967076687,~f6=711.25161146077,~f7=977.22674582697,~f8=1325.0082712923]; ~synthopen.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=1100.8670554563,~f5=1316.2675385788,~f6=1316.2675385788,~f7=1316.2675385788,~f8=1316.2675385788]; ~synthflow.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=1100.8670554563,~f5=1316.2675385788,~f6=1051.0271787501,~f7=1051.0271787501,~f8=1051.0271787501]; ~synthflow.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=1100.8670554563,~f5=1316.2675385788,~f6=937.1102649523,~f7=937.1102649523,~f8=937.1102649523]; ~synthflow.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=1100.8670554563,~f5=1316.2675385788,~f6=824.55305817848,~f7=824.55305817848,~f8=822.73593638317]; ~synthflow.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=1100.8670554563,~f5=1316.2675385788,~f6=824.55305817848,~f7=1037.2062139187,~f8=1037.2062139187]; ~synthflow.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=1100.8670554563,~f5=1316.2675385788,~f6=554.31101031015,~f7=554.31101031015,~f8=554.31101031015]; ~synthflow.value; ~trace.value; (a).wait;
			[~f1=654.05453931384,~f2=879.03193048766,~f3=941.25430167146,~f4=521.10543213641,~f5=521.10543213641,~f6=521.10543213641,~f7=521.10543213641,~f8=521.10543213641]; ~synthflow.value; ~trace.value; (a).wait;

			//fade out function
			100.do({~number_of_synths.do(x = 0; {("vol"++(x=x+1)).asSymbol.envirPut(("vol"++x).asSymbol.envirGet - (~vol/100))}); ~synthflow.value; 0.1.wait;});

			//stop everything
			~synthfree.value; 2.wait; s.stopRecording;
			~number_of_synths.do(x = 0; {("vol"++(x=x+1)).asSymbol.envirPut(~vol)});

});}););
t.play;
);

s.record; //2 - START RECORDING
s.stopRecording; // STOP RECORDING

(//1 - LOAD SYNTH FUNCTIONS
~synthopen = {
	case
	{((~l1a.isRunning == false) and: (~l1a1.isRunning == false)) or: ~l1a1.isRunning == true}{
		if(~l1a1.isRunning == true, {
			(1.do({
				s.makeBundle(0, {
					(~number_of_synths).do(x = 0; {
						("l"++(x=x+1)++"a1").asSymbol.envirGet.do(_.set(\dur, 0));
						("l"++(x)++"b1").asSymbol.envirGet.do(_.set(\dur, 0));
						("l"++(x)++"c1").asSymbol.envirGet.do(_.set(\dur, 0));
						("l"++(x)++"d1").asSymbol.envirGet.do(_.set(\dur, 0));
						("l"++(x)++"e1").asSymbol.envirGet.do(_.set(\dur, 0));
						("l"++(x)++"f1").asSymbol.envirGet.do(_.set(\dur, 0));
					});
				});
			}););
		});

		(
			1.do({
				6.do(x = 0; {("s"++(x=x+1)).asSymbol.envirPut([\gsineicfld6, \gsineicrld6, \gsineicfld2, \gsineicrld2, \gsineicfld1, \gsineicrld1].at(x-1))});
				case
				{~sendreplysynthdef_on == 0}{~number_of_synths.do(x = 0; {("s5_"++(x=x+1)).asSymbol.envirPut(\gsineicfld1)});}
				{~sendreplysynthdef_on == 1}{~number_of_synths.do(x = 0; {("s5_"++(x=x+1)).asSymbol.envirPut(\gsineicfld1_f++(x))});};
				s.makeBundle(0, {
					(~number_of_synths).do(x = 0; {
						("l"++(x=x+1)++"a").asSymbol.envirPut(Synth(~s1, [\dur, ("f"++(x)).asSymbol.envirGet,       \vol, ("vol"++(x)).asSymbol.envirGet]).register;);
						("l"++(x)++"b").asSymbol.envirPut(Synth(~s2,     [\dur, ("f"++(x)).asSymbol.envirGet,       \vol, ("vol"++(x)).asSymbol.envirGet]););
						("l"++(x)++"c").asSymbol.envirPut(Synth(~s3,     [\dur, ("f"++(x)).asSymbol.envirGet/~icd3, \vol, ("vol"++(x)).asSymbol.envirGet]););
						("l"++(x)++"d").asSymbol.envirPut(Synth(~s4,     [\dur, ("f"++(x)).asSymbol.envirGet/~icd3, \vol, ("vol"++(x)).asSymbol.envirGet]););
						("l"++(x)++"e").asSymbol.envirPut(Synth(("s5_"++(x)).asSymbol.envirGet,   [\dur, ("f"++(x)).asSymbol.envirGet/~icd6, \vol, ("vol"++(x)).asSymbol.envirGet]););
						("l"++(x)++"f").asSymbol.envirPut(Synth(~s6,     [\dur, ("f"++(x)).asSymbol.envirGet/~icd6, \vol, ("vol"++(x)).asSymbol.envirGet]););
					});
				});
			});
		);

		if(~l1a1.isRunning == true, {
			AppClock.sched(0.161803398875, {
				(x = 0; ((Array.fill(~number_of_synths, {[
					("l"++(x=x+1)++"a1").asSymbol.envirGet.do(_.free);
					("l"++(x)++"b1").asSymbol.envirGet.do(_.free);
					("l"++(x)++"c1").asSymbol.envirGet.do(_.free);
					("l"++(x)++"d1").asSymbol.envirGet.do(_.free);
					("l"++(x)++"e1").asSymbol.envirGet.do(_.free);
					("l"++(x)++"f1").asSymbol.envirGet.do(_.free);
				]}).flatten).do(_.free))); ~slideroutine.stop;
			});
		});
	}
	{~l1a.isRunning == true}{
		(1.do({
			s.makeBundle(0, {
				(~number_of_synths).do(x = 0; {
					("l"++(x=x+1)++"a").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"b").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"c").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"d").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"e").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"f").asSymbol.envirGet.do(_.set(\dur, 0));
				});
			});
		}););

		(
			1.do({
				6.do(x = 0; {("s"++(x=x+1)).asSymbol.envirPut([\gsineicfld6, \gsineicrld6, \gsineicfld2, \gsineicrld2, \gsineicfld1, \gsineicrld1].at(x-1))});
				case
				{~sendreplysynthdef_on == 0}{~number_of_synths.do(x = 0; {("s5_"++(x=x+1)).asSymbol.envirPut(\gsineicfld1)});}
				{~sendreplysynthdef_on == 1}{~number_of_synths.do(x = 0; {("s5_"++(x=x+1)).asSymbol.envirPut(\gsineicfld1_f++(x))});};
				s.makeBundle(0, {
					(~number_of_synths).do(x = 0; {
						("l"++(x=x+1)++"a1").asSymbol.envirPut(Synth(~s1, [\dur, ("f"++(x)).asSymbol.envirGet,       \vol, ("vol"++(x)).asSymbol.envirGet]).register;);
						("l"++(x)++"b1").asSymbol.envirPut(Synth(~s2,     [\dur, ("f"++(x)).asSymbol.envirGet,       \vol, ("vol"++(x)).asSymbol.envirGet]););
						("l"++(x)++"c1").asSymbol.envirPut(Synth(~s3,     [\dur, ("f"++(x)).asSymbol.envirGet/~icd3, \vol, ("vol"++(x)).asSymbol.envirGet]););
						("l"++(x)++"d1").asSymbol.envirPut(Synth(~s4,     [\dur, ("f"++(x)).asSymbol.envirGet/~icd3, \vol, ("vol"++(x)).asSymbol.envirGet]););
						("l"++(x)++"e1").asSymbol.envirPut(Synth(("s5_"++(x)).asSymbol.envirGet,   [\dur, ("f"++(x)).asSymbol.envirGet/~icd6, \vol, ("vol"++(x)).asSymbol.envirGet]););
						("l"++(x)++"f1").asSymbol.envirPut(Synth(~s6,     [\dur, ("f"++(x)).asSymbol.envirGet/~icd6, \vol, ("vol"++(x)).asSymbol.envirGet]););
					});
				});
			});
		);
		AppClock.sched(0.161803398875, {
			(x = 0; ((Array.fill(~number_of_synths, {[
				("l"++(x=x+1)++"a").asSymbol.envirGet.do(_.free);
				("l"++(x)++"b").asSymbol.envirGet.do(_.free);
				("l"++(x)++"c").asSymbol.envirGet.do(_.free);
				("l"++(x)++"d").asSymbol.envirGet.do(_.free);
				("l"++(x)++"e").asSymbol.envirGet.do(_.free);
				("l"++(x)++"f").asSymbol.envirGet.do(_.free);
			]}).flatten).do(_.free))); ~slideroutine.stop;
		});
	};
};

~synthflow = {
	case
	{~l1a1.isRunning == true}{
		(1.do({
			s.makeBundle(0, {
				(~number_of_synths).do(x = 0; {
					("l"++(x=x+1)++"a1").asSymbol.envirGet.do(_.set(\dur, ("f"++(x)).asSymbol.envirGet,       \vol, ("vol"++(x)).asSymbol.envirGet));
					("l"++(x)++"b1").asSymbol.envirGet.do(_.set(\dur,     ("f"++(x)).asSymbol.envirGet,       \vol, ("vol"++(x)).asSymbol.envirGet));
					("l"++(x)++"c1").asSymbol.envirGet.do(_.set(\dur,     ("f"++(x)).asSymbol.envirGet/~icd3, \vol, ("vol"++(x)).asSymbol.envirGet));
					("l"++(x)++"d1").asSymbol.envirGet.do(_.set(\dur,     ("f"++(x)).asSymbol.envirGet/~icd3, \vol, ("vol"++(x)).asSymbol.envirGet));
					("l"++(x)++"e1").asSymbol.envirGet.do(_.set(\dur,     ("f"++(x)).asSymbol.envirGet/~icd6, \vol, ("vol"++(x)).asSymbol.envirGet));
					("l"++(x)++"f1").asSymbol.envirGet.do(_.set(\dur,     ("f"++(x)).asSymbol.envirGet/~icd6, \vol, ("vol"++(x)).asSymbol.envirGet));
				});
			});
		}););
	}
	{~l1a.isRunning == true} {
		(1.do({
			s.makeBundle(0, {
				(~number_of_synths).do(x = 0; {
					("l"++(x=x+1)++"a").asSymbol.envirGet.do(_.set(\dur, ("f"++(x)).asSymbol.envirGet,       \vol, ("vol"++(x)).asSymbol.envirGet));
					("l"++(x)++"b").asSymbol.envirGet.do(_.set(\dur,     ("f"++(x)).asSymbol.envirGet,       \vol, ("vol"++(x)).asSymbol.envirGet));
					("l"++(x)++"c").asSymbol.envirGet.do(_.set(\dur,     ("f"++(x)).asSymbol.envirGet/~icd3, \vol, ("vol"++(x)).asSymbol.envirGet));
					("l"++(x)++"d").asSymbol.envirGet.do(_.set(\dur,     ("f"++(x)).asSymbol.envirGet/~icd3, \vol, ("vol"++(x)).asSymbol.envirGet));
					("l"++(x)++"e").asSymbol.envirGet.do(_.set(\dur,     ("f"++(x)).asSymbol.envirGet/~icd6, \vol, ("vol"++(x)).asSymbol.envirGet));
					("l"++(x)++"f").asSymbol.envirGet.do(_.set(\dur,     ("f"++(x)).asSymbol.envirGet/~icd6, \vol, ("vol"++(x)).asSymbol.envirGet));
				});
			});
		}););
	};
};

~synthslide = {
	case
	{~l1a1.isRunning == true}{(
		~slideroutine = Routine({
			1.do({
				#c,d,e,f,g,h,i,j,k,l,m,n = (([ o,p,q,r,s,t,u,v,w,x,y,z ]-[ ~f1,~f2,~f3,~f4,~f5,~f6,~f7,~f8,~f9,~f10,~f11,~f12 ])*(-1)/~slidedo);~slidedo.do({([ o+o=c,p+p=d,q+q=e,r+r=f,s=s+g,t=t+h,u=u+i,v=v+j,w=w+k,x=x+l,y=y+m,z=z+n ];
					s.makeBundle(0, {
						(~number_of_synths).do(x = 0; {
							("l"++(x=x+1)++"a1").asSymbol.envirGet.do(_.set(\dur, [ o,p,q,r,s,t,u,v,w,x,y,z ].at(x-1), \vol, ("vol"++(x)).asSymbol.envirGet));
							("l"++(x)++"b1").asSymbol.envirGet.do(_.set(\dur,     [ o,p,q,r,s,t,u,v,w,x,y,z ].at(x-1), \vol, ("vol"++(x)).asSymbol.envirGet));
							("l"++(x)++"c1").asSymbol.envirGet.do(_.set(\dur,     [ o,p,q,r,s,t,u,v,w,x,y,z ].at(x-1)/~icd3, \vol, ("vol"++(x)).asSymbol.envirGet));
							("l"++(x)++"d1").asSymbol.envirGet.do(_.set(\dur,     [ o,p,q,r,s,t,u,v,w,x,y,z ].at(x-1)/~icd3, \vol, ("vol"++(x)).asSymbol.envirGet));
							("l"++(x)++"e1").asSymbol.envirGet.do(_.set(\dur,     [ o,p,q,r,s,t,u,v,w,x,y,z ].at(x-1)/~icd6, \vol, ("vol"++(x)).asSymbol.envirGet));
							("l"++(x)++"f1").asSymbol.envirGet.do(_.set(\dur,     [ o,p,q,r,s,t,u,v,w,x,y,z ].at(x-1)/~icd6, \vol, ("vol"++(x)).asSymbol.envirGet));
						});
					});
				);
				(~slidetime/~slidedo).wait;
	});});}).play;);}
	{~l1a.isRunning == true} {(
		~slideroutine = Routine({
			1.do({
				#c,d,e,f,g,h,i,j,k,l,m,n = (([ o,p,q,r,s,t,u,v,w,x,y,z ]-[ ~f1,~f2,~f3,~f4,~f5,~f6,~f7,~f8,~f9,~f10,~f11,~f12 ])*(-1)/~slidedo);~slidedo.do({([ o+o=c,p+p=d,q+q=e,r+r=f,s=s+g,t=t+h,u=u+i,v=v+j,w=w+k,x=x+l,y=y+m,z=z+n ];
					s.makeBundle(0, {
						{
							(~number_of_synths).do(x = 0; {
								("l"++(x=x+1)++"a").asSymbol.envirGet.do(_.set(\dur, [ o,p,q,r,s,t,u,v,w,x,y,z ].at(x-1), \vol, ("vol"++(x)).asSymbol.envirGet));
								("l"++(x)++"b").asSymbol.envirGet.do(_.set(\dur,     [ o,p,q,r,s,t,u,v,w,x,y,z ].at(x-1), \vol, ("vol"++(x)).asSymbol.envirGet));
								("l"++(x)++"c").asSymbol.envirGet.do(_.set(\dur,     [ o,p,q,r,s,t,u,v,w,x,y,z ].at(x-1)/~icd3, \vol, ("vol"++(x)).asSymbol.envirGet));
								("l"++(x)++"d").asSymbol.envirGet.do(_.set(\dur,     [ o,p,q,r,s,t,u,v,w,x,y,z ].at(x-1)/~icd3, \vol, ("vol"++(x)).asSymbol.envirGet));
								("l"++(x)++"e").asSymbol.envirGet.do(_.set(\dur,     [ o,p,q,r,s,t,u,v,w,x,y,z ].at(x-1)/~icd6, \vol, ("vol"++(x)).asSymbol.envirGet));
								("l"++(x)++"f").asSymbol.envirGet.do(_.set(\dur,     [ o,p,q,r,s,t,u,v,w,x,y,z ].at(x-1)/~icd6, \vol, ("vol"++(x)).asSymbol.envirGet));
							});
						}
					});
				);
				(~slidetime/~slidedo).wait;});});
}).play;);};};


~synthpause = {(
	case
	{~l1a1.isRunning == true}{
		(1.do({
			s.makeBundle(0, {
				(~number_of_synths).do(x = 0; {
					("l"++(x=x+1)++"a1").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"b1").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"c1").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"d1").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"e1").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"f1").asSymbol.envirGet.do(_.set(\dur, 0));
				});
			});
		}););
	}
	{~l1a.isRunning == true} {
		(1.do({
			s.makeBundle(0, {
				(~number_of_synths).do(x = 0; {
					("l"++(x=x+1)++"a").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"b").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"c").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"d").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"e").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"f").asSymbol.envirGet.do(_.set(\dur, 0));
				});
			});
		}););
	};
);
};

~synthfree = {
	(
		case
		{~l1a1.isRunning == true}{
			(1.do({
				(~number_of_synths).do(x = 0; {
					("l"++(x=x+1)++"a1").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"b1").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"c1").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"d1").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"e1").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"f1").asSymbol.envirGet.do(_.set(\dur, 0));
				});
			}););
			AppClock.sched(0.161803398875, {
				(x = 0; ((Array.fill(~number_of_synths, {[
					("l"++(x=x+1)++"a1").asSymbol.envirGet.do(_.free);
					("l"++(x)++"b1").asSymbol.envirGet.do(_.free);
					("l"++(x)++"c1").asSymbol.envirGet.do(_.free);
					("l"++(x)++"d1").asSymbol.envirGet.do(_.free);
					("l"++(x)++"e1").asSymbol.envirGet.do(_.free);
					("l"++(x)++"f1").asSymbol.envirGet.do(_.free);
				]}).flatten).do(_.free))); ~slideroutine.stop;
			});
		}
		{~l1a.isRunning == true}{
			(1.do({
				(~number_of_synths).do(x = 0; {
					("l"++(x=x+1)++"a").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"b").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"c").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"d").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"e").asSymbol.envirGet.do(_.set(\dur, 0));
					("l"++(x)++"f").asSymbol.envirGet.do(_.set(\dur, 0));
				});
			}););
			AppClock.sched(0.161803398875, {
				(x = 0; ((Array.fill(~number_of_synths, {[
					("l"++(x=x+1)++"a").asSymbol.envirGet.do(_.free);
					("l"++(x)++"b").asSymbol.envirGet.do(_.free);
					("l"++(x)++"c").asSymbol.envirGet.do(_.free);
					("l"++(x)++"d").asSymbol.envirGet.do(_.free);
					("l"++(x)++"e").asSymbol.envirGet.do(_.free);
					("l"++(x)++"f").asSymbol.envirGet.do(_.free);
				]}).flatten).do(_.free))); ~slideroutine.stop;
			});
		};
	);
};
);

/* e-mail: ken_brant@ymail.com */